package ru.t1.skasabov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.ApplicationHostNameRequest;
import ru.t1.skasabov.tm.dto.response.ApplicationHostNameResponse;

@Component
@NoArgsConstructor
public final class ApplicationHostNameCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "host";

    @NotNull
    private static final String DESCRIPTION = "Show host name.";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[HOST]");
        @NotNull final ApplicationHostNameRequest request = new ApplicationHostNameRequest();
        @NotNull final ApplicationHostNameResponse response = systemEndpoint.getHostName(request);
        System.out.println(response.getHostName());
    }

}
