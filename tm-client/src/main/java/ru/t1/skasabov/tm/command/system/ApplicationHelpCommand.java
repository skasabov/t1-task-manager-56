package ru.t1.skasabov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.api.model.ICommand;
import ru.t1.skasabov.tm.command.AbstractCommand;

import java.util.Collection;

@Component
@NoArgsConstructor
public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "help";

    @NotNull
    private static final String DESCRIPTION = "Show list arguments.";

    @NotNull
    private static final String ARGUMENT = "-h";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        for (@NotNull final ICommand command : commands) System.out.println(command);
    }

}
