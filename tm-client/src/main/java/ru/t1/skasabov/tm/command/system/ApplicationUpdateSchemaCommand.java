package ru.t1.skasabov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.ApplicationUpdateSchemaRequest;
import ru.t1.skasabov.tm.util.TerminalUtil;

@Component
@NoArgsConstructor
public final class ApplicationUpdateSchemaCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "update-schema";

    @NotNull
    private static final String DESCRIPTION = "Update database schema.";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE SCHEMA]");
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        @NotNull final ApplicationUpdateSchemaRequest request = new ApplicationUpdateSchemaRequest(password);
        systemEndpoint.updateSchema(request);
    }

}
