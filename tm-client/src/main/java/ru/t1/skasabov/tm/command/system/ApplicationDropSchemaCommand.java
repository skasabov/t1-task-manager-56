package ru.t1.skasabov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.ApplicationDropSchemaRequest;
import ru.t1.skasabov.tm.util.TerminalUtil;

@Component
@NoArgsConstructor
public final class ApplicationDropSchemaCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "drop-schema";

    @NotNull
    private static final String DESCRIPTION = "Drop database schema.";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DROP SCHEMA]");
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        @NotNull final ApplicationDropSchemaRequest request = new ApplicationDropSchemaRequest(password);
        systemEndpoint.dropSchema(request);
    }

}
