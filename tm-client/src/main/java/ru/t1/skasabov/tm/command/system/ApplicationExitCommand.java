package ru.t1.skasabov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public final class ApplicationExitCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "exit";

    @NotNull
    private static final String DESCRIPTION = "Close application.";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
