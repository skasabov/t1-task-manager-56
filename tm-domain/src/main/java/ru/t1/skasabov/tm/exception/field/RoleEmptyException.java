package ru.t1.skasabov.tm.exception.field;

import ru.t1.skasabov.tm.exception.user.AbstractUserException;

public final class RoleEmptyException extends AbstractUserException {

    public RoleEmptyException() {
        super("Error! Role is empty...");
    }

}
