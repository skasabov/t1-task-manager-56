package ru.t1.skasabov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.skasabov.tm.api.service.IAuthService;
import ru.t1.skasabov.tm.dto.request.AbstractUserRequest;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.exception.user.PermissionException;
import ru.t1.skasabov.tm.dto.model.SessionDTO;

@Getter
@Setter
@Controller
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    protected IAuthService authService;

    @NotNull
    protected SessionDTO check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new PermissionException();
        if (role == null) throw new PermissionException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new PermissionException();
        @NotNull final SessionDTO session = authService.validateToken(token);
        if (!session.getRole().equals(role)) throw new PermissionException();
        return session;
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new PermissionException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new PermissionException();
        return authService.validateToken(token);
    }

}
