package ru.t1.skasabov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public interface IDTORepository<M extends AbstractModelDTO> {

    @NotNull
    EntityManager getEntityManager();

    void add(@NotNull M model);

    void update(@NotNull M model);

    @NotNull
    List<M> findAll();

    @NotNull
    Collection<M> addAll(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    Boolean existsById(@NotNull String id);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    long getSize();

    void removeOne(@NotNull M model);

    void removeOneById(@NotNull String id);

    void removeOneByIndex(@NotNull Integer index);

    void removeAll(@NotNull Collection<M> collection);

    void removeAll();

}
