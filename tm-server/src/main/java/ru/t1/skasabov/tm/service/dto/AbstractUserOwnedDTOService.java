package ru.t1.skasabov.tm.service.dto;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.skasabov.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.skasabov.tm.dto.model.AbstractUserOwnedModelDTO;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO>
        extends AbstractDTOService<M> implements IUserOwnedDTOService<M> {

}
