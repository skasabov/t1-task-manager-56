package ru.t1.skasabov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.skasabov.tm.api.repository.model.IProjectRepository;
import ru.t1.skasabov.tm.api.service.model.IProjectService;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.skasabov.tm.exception.entity.UserNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.*;

@Service
@NoArgsConstructor
public final class ProjectService extends AbstractUserOwnedService<Project> implements IProjectService {

    @NotNull
    public IProjectRepository getRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final Collection<Project> collection) {
        if (collection == null) return;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeAll(collection);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<Project> addAll(@Nullable final Collection<Project> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<Project> set(@Nullable final Collection<Project> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.set(models);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return models;
    }

    @NotNull
    @Override
    public Project add(@Nullable final Project model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull List<Project> projects;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            projects = repository.findAll();
        }
        finally {
            entityManager.close();
        }
        return projects;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final Sort sortType) {
        if (sortType == null) return findAll();
        @NotNull List<Project> projects;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            switch(sortType) {
                case BY_CREATED:
                    projects = repository.findAllSortByCreated();
                    break;
                case BY_STATUS:
                    projects = repository.findAllSortByStatus();
                    break;
                case BY_NAME:
                    projects = repository.findAllSortByName();
                    break;
                default:
                    projects = Collections.emptyList();
                    break;
            }
        }
        finally {
            entityManager.close();
        }
        return projects;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Project project;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            project = repository.findOneById(id);
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable Project project;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            project = repository.findOneByIndex(index);
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project removeOne(@Nullable final Project model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOne(model);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project model = findOneById(id);
        if (model == null) return null;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(id);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable final Project model = findOneByIndex(index);
        if (model == null) return null;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneByIndex(index);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        long size;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            size = repository.getSize();
        }
        finally {
            entityManager.close();
        }
        return size;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsProject;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            existsProject = repository.existsById(id);
        }
        finally {
            entityManager.close();
        }
        return existsProject;
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeAll(userId);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsProject;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            existsProject = repository.existsById(userId, id);
        }
        finally {
            entityManager.close();
        }
        return existsProject;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<Project> projects;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            projects = repository.findAll(userId);
        }
        finally {
            entityManager.close();
        }
        return projects;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId, @Nullable final Sort sortType) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sortType == null) return findAll(userId);
        @NotNull List<Project> projects;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            switch(sortType) {
                case BY_CREATED:
                    projects = repository.findAllSortByCreatedForUser(userId);
                    break;
                case BY_STATUS:
                    projects = repository.findAllSortByStatusForUser(userId);
                    break;
                case BY_NAME:
                    projects = repository.findAllSortByNameForUser(userId);
                    break;
                default:
                    projects = Collections.emptyList();
                    break;
            }
        }
        finally {
            entityManager.close();
        }
        return projects;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Project project;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            project = repository.findOneById(userId, id);
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable Project project;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            project = repository.findOneByIndex(userId, index);
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        long size;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            size = repository.getSize(userId);
        }
        finally {
            entityManager.close();
        }
        return size;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project model = findOneById(userId, id);
        if (model == null) return null;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Project model = findOneByIndex(userId, index);
        if (model == null) return null;
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneByIndex(userId, index);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        @NotNull final Project project = new Project();
        project.setUser(user);
        project.setName(name);
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        @NotNull final Project project = new Project();
        project.setUser(user);
        project.setName(name);
        project.setDescription(description);
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        if (dateBegin == null || dateEnd == null) return create(userId, name, description);
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        @NotNull final Project project = new Project();
        project.setUser(user);
        project.setName(name);
        project.setDescription(description);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return project;
    }

}
